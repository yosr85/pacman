using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public GameObject CoinsPrefab;
    public float turnSpeed = 90f;
    void Start()
    {
        //Instantiate(CoinPrefab);
        //Instantiate(CoinPrefab, new Vector3(0, 0.51f, 6), CoinPrefab.transform.rotation);  // r�cup�rer l'orientation de prefab
        Instantiate(CoinsPrefab, new Vector3(0, -1.97f, 6), Quaternion.identity);  // quaterion.identity --> l'orientation du rep�re global
        //essayer de tester les methodes de recherche
        //Nom:
        GameObject go1 = GameObject.Find("player");
        // Tag
        GameObject go2 = GameObject.FindGameObjectWithTag("Floor");
        // retourne un ensemble de gameObjects avec tag
        GameObject[] array1 = GameObject.FindGameObjectsWithTag("Floor");
        // Recherche par component
        player pc1 = FindObjectOfType<player>();
        // Recherche par component
       player[] array2 = FindObjectsOfType<player>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.right * turnSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Coins"))
        {
            Destroy(other.gameObject);
        }
    }
}
